Feature: To test the login functionality

  Scenario Outline: Check login is successfull with valid credentials
    Given browser is open
    And user is on login page
    When user enter <username> and <password>
    And user clicks on login
    Then user is navigate to home page

    Examples: 
      | username  | password |
      | Swapnalee |    12345 |
      | Ele       |      123 |
