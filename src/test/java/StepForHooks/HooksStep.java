package StepForHooks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HooksStep {
	
	WebDriver driver = null;
	@Before(order=1)
	public void browserSetup() {
		System.out.println("BrowserSetup1 is open");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\RAM\\eclipse-workspace\\CucumberProject1\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	@Before(order=0)
	public void browserSetup2() {
		System.out.println("BrowserSetup2 is open");	
	}
	@Given("^user is on login page$")
	public void user_is_on_login_page() {
	}

	@BeforeStep
	public void beforeSteps(){
		System.out.println("I am inside before steps........");
	}
	@AfterStep
	public void afterSteps() {
		System.out.println("I am inside after steps...........");
	}
	
	@When("^user enters valid username and password$")
	public void user_enters_valid_username_and_password(){
	}

	@And("^clicks on login button$")
	public void clicks_on_login_button() {
	}

	@Then("^user is navigated to homepage$")
	public void user_is_navigated_to_homepage(){
	}
	
	@After(order=1)
	public void BrowserClose() {
		
		driver.close();
		driver.quit();
		System.out.println("Browser is close - 1");
	}
	@After(order=2)
	public void Browserclose2() {
		System.out.println("Browser is close - 2");
	}
}
