//package StepDefinations;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class loginDemoStepDef {
//	WebDriver driver=null;
//	@Given("^browser is open$")
//	public void browser_is_open() {
//		String projectpath = System.getProperty("user.dir");
//		System.out.println("Project Path is:" + projectpath);
//		
//		System.setProperty("webdriver.gecko.driver", projectpath + "\\Drivers\\geckodriver.exe");
//		driver = new FirefoxDriver();
//		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
//		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
//	}
//	@And("^user is on login page$")
//	public void user_is_on_login_page()  {
//		driver.navigate().to("https://example.testproject.io/web/");
//	}
//
//	@When("^user enter (.*) and (.*)$")
//	public void user_enter_username_and_password(String username, String password) {
//		driver.findElement(By.id("name")).sendKeys(username);
//		driver.findElement(By.id("password")).sendKeys(password);
//	}
//	@And("^user clicks on login$")
//	public void user_clicks_login() {
//		driver.findElement(By.id("login")).click();
//	}
//	@Then("^user is navigate to home page\\.$")
//	public void user_is_navigate_to_home_page(){
//		driver.findElement(By.id("logout")).isDisplayed();
//		driver.close();
//		driver.quit();
//	}
//
//}
