package StepDefinations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class googleSearchSteps {
	WebDriver driver=null;
	
	@Given("^browser is open$")
	public void browser_is_open() {
		String projectpath = System.getProperty("user.dir");
		System.out.println("Project Path is:" + projectpath);
		
		System.setProperty("webdriver.chrome.driver", projectpath + "\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	}

	@And("^user is on google search page$")
	public void user_is_on_google_search_page() {
		driver.navigate().to("https://google.com");
		
	}

	@When("^user enters a text in search box$")
	public void user_enters_a_text_in_search_box() {
		driver.findElement(By.xpath("//body/div[@id='viewport']/div[@id='searchform']/form[@id='tsf']/div[2]/div[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys("Automation step by step");
	}

	@And("^hits enter$")
	public void hits_enter() throws Throwable {
		driver.findElement(By.xpath("//body/div[@id='viewport']/div[@id='searchform']/form[@id='tsf']/div[2]/div[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys(Keys.ENTER);
	}

	@Then("^user is navigated to search results$")
	public void user_is_navigated_to_search_results() throws Throwable {
	   driver.getPageSource().contains("Online Courses");
	   driver.close();
	   driver.quit();
	}
}