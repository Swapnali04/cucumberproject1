package Runner;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = "C:\\Users\\RAM\\eclipse-workspace\\CucumberProject1\\Feature\\HOOKS.feature" ,
	glue="StepForHooks",
	dryRun = true,
	plugin= {"pretty", "html:test-output"},
	monochrome = true
 )


public class testRunforHooks {

}
